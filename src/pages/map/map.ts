import { Component, ViewChild, ElementRef } from '@angular/core';

import { ConferenceData } from '../../providers/conference-data';

import { Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google: any;


@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {

  @ViewChild('mapCanvas') mapElement: ElementRef;
  constructor(public confData: ConferenceData, public platform: Platform, public geolocation: Geolocation) {
  }

  ionViewDidLoad() {
 
      this.confData.getMap().subscribe((mapData: any) => {
        this.geolocation.getCurrentPosition().then((position) => {//

        let mapEle = this.mapElement.nativeElement;
      
        let map = new google.maps.Map(mapEle, {
          //center: mapData.find((d: any) => d.center),          
          center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
          zoom: 16
        });

        //this.geolocation.getCurrentPosition().then((position) => {
           let infoWindow = new google.maps.InfoWindow({
            content: `<h5>YO</h5>`
          });

          let marker = new google.maps.Marker({
            icon : 'assets/img/marker.png',
            position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
            map: map,
            title: 'YO'
          });

          marker.addListener('click', () => {
            infoWindow.open(map, marker);
          });//FIN
        /*}, (err) => {
          console.log(err);
        });*/
      
        
        mapData.forEach((markerData: any) => {
          let infoWindow = new google.maps.InfoWindow({
            content: `<h5>${markerData.name}</h5>`
          });

          let marker = new google.maps.Marker({
            icon : 'assets/img/miMarker.png',
            position: markerData,
            map: map,
            title: markerData.name
          });

          marker.addListener('click', () => {
            infoWindow.open(map, marker);
          });
        });        

        google.maps.event.addListenerOnce(map, 'idle', () => {
          mapEle.classList.add('show-map');
        });

        }, (err) => {
          console.log(err);
        });

      });

  }
}
